import Chart from "chart.js";

var lineChartData = {
  labels: [],
  datasets: [{
    label: '# Sensor',
    data: [],
    backgroundColor: "#f6821f",
    hoverBackgroundColor: "rgba(255,99,132,0.4)",
    hoverBorderColor: "rgba(255,99,132,1)",
    borderColor: "rgba(255,99,132,0.4)",
    backgroundColor: "transparent",
    borderWidth: 2,
}]
};
function lineChart(sensor){
  let label = [],sensorData= [];
  let resultSet = sensor.map((data, i) => {
    var timestamp = new Date(data.timestamp * 1000);
    var month=(timestamp.getMonth())+1;
    label.push(timestamp.getDate()+'-'+month+'-'+timestamp.getFullYear());
    sensorData.push(data.reading);
  });
  lineChartData.datasets[0].data = sensorData;
  lineChartData.labels = label;
  var ctx = document.getElementById('myChart');
   window.myChart = new Chart(ctx, {
      type: 'line',
      data:lineChartData,
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
}

export default lineChart  ;