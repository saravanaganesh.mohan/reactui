import React from 'react';
import './react-table.css';
import './styles.css';
import './bootstrap.min.css'
import axios from 'axios';
import DatePicker from "react-datepicker";
import { format, isDate } from "date-fns";
import './react-datepicker.min.css';
import lineChart from "./SensorChart";
class SensorList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        sensorList:[],
        sensorType:"",
        url:"http://localhost:8000/api/sensor",
        startDate:"",
        endDate:"",
        clearFlag:0
    };  
  }

submitForm=()=>{
    const {sensorType,startDate,endDate}=this.state;
    const url="http://localhost:8000/api/sensor"
    console.log("startDate---",startDate);
    const startDateDt=startDate/1000;
    const endDateDt=endDate/1000;
    if (sensorType !=="") {
        this.setState({url:url+"?sensorType="+sensorType,clearFlag:0},()=>{
            this.getSensorData();
        })
      }
      if (startDate !=="" && endDate!=="") {
          if(sensorType !==""){
            this.setState({url:url+"?sensorType="+sensorType+"&startDate="+startDateDt+"&endDate="+endDateDt,clearFlag:0},()=>{
                this.getSensorData();
            })
          }else{
            this.setState({url:url+"?startDate="+startDateDt+"&endDate="+endDateDt,clearFlag:0},()=>{
                this.getSensorData();
            })
          }
        
      }
}
clearForm=()=>{
    this.setState({
        sensorList:[],
        sensorType:"",
        url:"http://localhost:8000/api/sensor",
        startDate:"",
        endDate:"",
        clearFlag:1
    },()=>{
        this.getSensorData();
    })
}
getSensorData(){
    axios.get(this.state.url)
    .then(res => {
      const sensor = res.data;
      if(this.state.clearFlag===0){
        lineChart(sensor);
      }
      this.setState({sensorList:sensor});
     console.log(sensor);
    })
}
  componentDidMount() {
    this.getSensorData();
  }
  render() {
    const sensorList = this.state.sensorList;
    const {sensorType,startDate,endDate}=this.state;
    let startDateDt = new Date(startDate);
    let endDateDt = new Date(endDate);
    return (
      <>
      <>
            <div className="row align-items-center no-gutters">
              <div className="col-md-12">
                <h1 className="heading m-b-0">Sensor List</h1>
              </div>
            </div>
            <div className="row marginLeft marginTop">
                <div className="col-md-2">
                    <label>
                        Sensor Type
                        </label>
                </div>
            <div className="col-md-2">
                <input
                      type="text"
                      value={sensorType}
                      onChange={e => {
                        this.setState({
                            sensorType: e.target.value
                        });
                      }}
                    />
                </div>
                <div className="col-md-1">
                    <label>
                    Start Date
                    </label>
                </div>
                <div className="col-md-2">
                <DatePicker
                      dateFormat="dd-MM-yyyy"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      placeholderText="DD-MM-YYYY"
                      readonly
                      className=""
                      selected={startDate === "" ? null : startDateDt}
                      onChange={dt => {
                        if (isDate(dt)) {
                          this.setState({ 
                            startDate: new Date(dt).setHours(0,0,0)
                          });
                        } else {
                          this.setState({ startDate: "" });
                        }
                      }}
                    />
                </div>
                <div className="col-md-1">
                    <label>
                    End Date
                    </label>
                </div>
                <div className="col-md-2">
                <DatePicker
                      dateFormat="dd-MM-yyyy"
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      placeholderText="DD-MM-YYYY"
                      readonly
                      className=""
                      selected={endDate === "" ? null : endDateDt}
                      onChange={dt => {
                        if (isDate(dt)) {
                          this.setState({ 
                            endDate: new Date(dt).setHours(23,59,59)  
                          });
                        } else {
                          this.setState({ endDate: "" });
                        }
                      }}
                    />
                </div>
            <div className="col-md-1">
              <button
                type="button"
                className="btn btn-success"
                onClick={this.submitForm}
              >
                Apply
              </button>
            </div>
            <div className="col-md-1">
              <button
                type="button"
                className="btn btn-light"
                onClick={this.clearForm}
              >
                Clear
              </button>
            </div>
                </div>
            <div className="marginLeft">
              <table className="table table-style-1">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Reading</th>
                    <th>Date Time</th>
                    <th>Sensor Type</th>
                  </tr>
                </thead>

                <tbody>
                  {sensorList.length
                    ? sensorList.map((data, index) => {
                        var timestamp = new Date(data.timestamp * 1000);  
                        var month=(timestamp.getMonth())+1;                      
                        return (
                          <tr key={index}>
                            <td>{data.id} </td>
                            <td>{data.reading}</td>
                            <td>{timestamp.getDate()+'-'+month+'-'+timestamp.getFullYear()} </td>
                            <td>{data.sensorType}</td>
                          </tr>
                        );
                      })
                    : null}
                </tbody>
              </table>
            </div>
            {!sensorList.length ? (
              <div style={{ textAlign: "center" }}>No Data</div>
            ) : null}
      </>
      <div className="row marginLeft marginTop">
          <div className="col-md-10">
      <canvas id="myChart" height="120px"
      style={{ height: "120px !important" , width:"200px !important"}} >
      </canvas>
      </div>
      </div>
      </>
    );
  }
}
export default SensorList;
